from enum import Enum
import pyglet

from hex import *
from etypes import *
from animation import *

player_images = [pyglet.resource.image('player_1.png'), pyglet.resource.image('player_2.png')]
player_anim = pyglet.image.Animation.from_image_sequence(player_images, duration=0.7, loop=True)

player_unconscious_image = pyglet.resource.image('player_1.png')
player_dead_image = pyglet.resource.image('player_dead_1.png')

enemy1_images = [pyglet.resource.image('enemy1_1.png'), pyglet.resource.image('enemy1_2.png')]
enemy1_anim = pyglet.image.Animation.from_image_sequence(enemy1_images, duration=0.7, loop=True)

enemy1_unconscious_image = pyglet.resource.image('enemy1_1.png')

enemy1_dead_images = [pyglet.resource.image('enemy1_dead_1.png'), pyglet.resource.image('enemy1_dead_2.png'), pyglet.resource.image('enemy1_dead_3.png')]
enemy1_dead_anim = pyglet.image.Animation.from_image_sequence(enemy1_dead_images, duration=0.1, loop=False)

# Entity class
class Entity:
    def __init__(self, maptile, type, align, state, on_die):
        self.on_die = on_die
        self.maptile = maptile
        self.animation = None
        
        self.type = type
        self.align = align
        self.state = state
        self.incapacitation_timer = 0
        self.set_image()

    def set_state(self, state):
        self.state = state
        self.set_image()
		
    def set_image(self):
        self.image = None
        if self.type == EntityType.PLAYER:
            if self.state == EntityState.CONSCIOUS:
                self.image = pyglet.sprite.Sprite(img=player_anim)
            elif self.state == EntityState.UNSCONSCIOUS:
                self.image = pyglet.sprite.Sprite(img=player_unconscious_image)
            elif self.state == EntityState.DEAD:
                self.image = pyglet.sprite.Sprite(img=player_dead_image)
        elif self.type == EntityType.ENEMY1:
            if self.state == EntityState.CONSCIOUS:
                self.image = pyglet.sprite.Sprite(img=enemy1_anim)
            elif self.state == EntityState.UNSCONSCIOUS:
                self.image = pyglet.sprite.Sprite(img=enemy1_unconscious_image)
            elif self.state == EntityState.DEAD:
                self.image = pyglet.sprite.Sprite(img=enemy1_dead_anim)

    def attack(self, target, type):
        if target and target.state != EntityState.DEAD and type in [AttackType.PUNCH, AttackType.CLAW]:
            # Change sprite temporarily, look at Sprite.on_animation_end()
            # ...
            return target.on_attacked(self, type)
        elif type == AttackType.PUSH:
            return target.on_pushed(hex_subtract(target.maptile.hex, self.maptile.hex))
        elif type == AttackType.WHIRLWIND:
            acted = False
            for i in range(6):
                dir = hex_direction(i)
                hex = hex_add(self.maptile.hex, dir)
                maptile = self.maptile.map.get_tile((hex.q, hex.r))
                if maptile and maptile.entity:
                    entity = maptile.entity
                    if entity.on_pushed(hex_subtract(entity.maptile.hex, self.maptile.hex), anim_entity_whirlwind_time + 0.5):
                        acted = True
            
            # Add charge-up animation
            if acted:
                self.animation = Animation(MovementType.WHIRLWIND_CHARGEUP, self.maptile, self.maptile)
                
            return acted
            
    def interact_with(self, target):
        return False
		
    def die(self):
        self.state = EntityState.DEAD
        self.set_image()
        self.on_die()
        
    def on_attacked(self, attacker, type):
        self.die()
        return True
        
    def on_pushed(self, dir, animdelay=0):
        if self.maptile.map.move_entity(self, dir, MovementType.PUSHED, animdelay):
            self.incapacitation_timer += 1
            return True
        else:
            return False