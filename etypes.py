from enum import Enum, auto

class EntityType(Enum):
    PLAYER = auto()
    ENEMY1 = auto()
	
class AlignType(Enum):
    FRIENDLY = auto()
    HOSTILE = auto()
    NEUTRAL = auto()

class AttackType(Enum):
    PUNCH = auto()
    PUSH = auto()
    WHIRLWIND = auto()
    CLAW = auto() # Enemy attack

class EntityState(Enum):
    CONSCIOUS = auto()
    UNSCONSCIOUS = auto()
    DEAD = auto()
    
class MovementType(Enum):
    NONE = auto()
    WALKING = auto()
    LEAPING = auto()
    DASHING = auto()
    PUSHED = auto()
    WHIRLWIND_CHARGEUP = auto()
