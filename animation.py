import math

from hex import *

from etypes import *

# Animation timings
anim_entity_walking_time = 0.5
anim_entity_leaping_time = 1.0
anim_entity_dashing_time = 1.0
anim_entity_pushed_time = 0.5
anim_entity_whirlwind_time = 1.0
anim_entity_leap_height = 36
anim_entity_whirlwind_charge_height = 48

anim_post_anim_pause_time = 0.25

#
# Easing functions
#
def ease_none(dt):
    return dt
    
def ease_out_expo(dt):
    return 1.0 if dt == 1.0 else 1.0 - math.pow(2, -10 * dt)

def ease_out_elastic(dt):
    c4 = 2 * math.pi / 3.0
    return 0.0 if dt == 0.0 else 1.0 if dt == 1.0 else math.pow(2, -10 * dt) * math.sin((dt * 3 - 0.75) * c4) + 1.0
    
def ease_inverse_cosine(dt):
    return (1.0 - math.cos(dt * 2 * math.pi)) * 0.5 + 0.5

def ease_gravity_slam(dt):
    f = 0.75
    if dt < f:
        return dt / f
    else:
        return (1.0 - dt) / (1.0 - f)
    
#
# Animation wrapper class
#
class Animation:
    def __init__(self, type, oldtile, newtile, delay=0):
        self.time = -delay
        self.type = type
        self.oldtile = oldtile
        self.newtile = newtile
        
        if type == MovementType.WALKING:
            self.length = anim_entity_walking_time
        elif type == MovementType.LEAPING:
            self.length = anim_entity_leaping_time
        elif type == MovementType.DASHING:
            self.length = anim_entity_dashing_time
        elif type == MovementType.PUSHED:
            self.length = anim_entity_pushed_time
        elif type == MovementType.WHIRLWIND_CHARGEUP:
            self.length = anim_entity_whirlwind_time
        else:
            raise ValueError(type)
            
    def dt(self):
        delta = self.time / self.length

        if self.type == MovementType.WALKING:
            return ease_none(delta)
        elif self.type == MovementType.LEAPING:
            return ease_none(delta)
        elif self.type == MovementType.DASHING:
            return ease_out_elastic(delta)
        elif self.type == MovementType.PUSHED:
            return ease_out_expo(delta)
        elif self.type == MovementType.WHIRLWIND_CHARGEUP:
            return ease_inverse_cosine(delta)
            
    def lerp_pos(self, layout):
        dt = self.dt()
        
        p0 = hex_to_pixel(layout, self.oldtile.hex)
        p1 = hex_to_pixel(layout, self.newtile.hex)
        
        if self.type in [MovementType.WALKING, MovementType.DASHING, MovementType.PUSHED]:
            ex = int(p0.x + (p1.x - p0.x) * dt)
            ey = int(p0.y + (p1.y - p0.y) * dt)
        elif self.type == MovementType.LEAPING:
            ex = int(p0.x + (p1.x - p0.x) * dt)
            ey = int(p0.y + (p1.y - p0.y) * dt) + ease_inverse_cosine(dt) * anim_entity_leap_height
        elif self.type == MovementType.WHIRLWIND_CHARGEUP:
            ex = p0.x
            ey = p0.y + anim_entity_whirlwind_charge_height * dt
        
        return ex, ey
