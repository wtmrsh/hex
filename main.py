import sys
import math
import random
from enum import Enum, auto
from threading import Lock

import pyglet
from pyglet.gl import *
from pyglet.window import key

from hex import *
from entity import *
from map import *
from etypes import *
from animation import *

# Window and gui
window = pyglet.window.Window(width=800, height=600)
action_points_text = pyglet.text.Label('AP', font_name='Times New Roman', font_size=18, x=10, y=window.height - 28)
restart_text = pyglet.text.Label("Hit 'R' to restart", font_name='Times New Roman', font_size=18, x=10, y=10)

# Images
tile_image = pyglet.resource.image('hex52.png')

# Render options
tile_size = 30
layout = Layout(layout_flat, Point(tile_size, tile_size), Point(window.width / 2, window.height / 2))

class GameState(Enum):
    WaitingForInput = auto()
    UpdatingPlayerAnimations = auto()
    PostAnimPause = auto()
    PerformingAIlogic = auto()
    UpdatingAIAnimations = auto()
    PlayerDead = auto()

# Game variables
player = None
game_map = None
num_action_points = 0

map_radius = 5

move_action_points = 1
punch_action_points = 1
push_action_points = 1
whirlwind_action_points = 2
leap_action_points = 2
dash_action_points = 2

game_state = GameState.WaitingForInput
game_lock = Lock()



#   
#######################################
#    
# Manage player action points.
#
#######################################
#
def reset_action_points():
    global num_action_points
    num_action_points = 1

def use_action_points(points):
    global num_action_points
    num_action_points -= points
    
def add_action_points(points):
    global num_action_points
    num_action_points += points

    
    
#   
#######################################
#
# setup_map()
#    
# Create the level and set up entities.
#
#######################################
#
def setup_map(radius):
    global player, game_map
	
    reset_action_points()
    
	# Generate map
    game_map = Map()
    
    zmin = 1 - radius
    zmax = 1 - zmin

    # Death functions
    game_over = lambda: None
    enemy_die = lambda: add_action_points(2)
    
    entity_spots = [(-3, -0), (3, 1), (2, -3), (2, -2), (-3, 3), (2, 1), (-1, -3), (2, 2)]
    for x in range(1 - radius, radius):
        if x <= 0:
            z0 = zmin - x
            z1 = zmax
        else:
            z0 = zmin
            z1 = zmax - x
			
        for z in range(z0, z1):
        
            if random.random() < 0.85 or (x == 0 and z == 0) or ((x, z) in entity_spots):
                game_map.add_tile(x, z)

            if x == 0 and z == 0:
                player = game_map.add_entity(x, z, EntityType.PLAYER, AlignType.FRIENDLY, EntityState.CONSCIOUS, game_over)
				
    # Add enemies
    for coord in entity_spots:
        game_map.add_entity(coord[0], coord[1], EntityType.ENEMY1, AlignType.HOSTILE, EntityState.CONSCIOUS, enemy_die)

        
        
#   
#######################################
#
# update_frame()
#    
# Run animations for player and other
# entities.
#
#######################################
#
post_anim_pause_time = 0.0

def update_frame(freq, dt):
    global game_state, post_anim_pause_time, num_action_points
    
    if not game_lock.acquire(blocking=False):
        return

    # See whether we're pausing between finishing player animations and the rest of the entities thinking
    if game_state == GameState.PostAnimPause:
        post_anim_pause_time += dt
        if post_anim_pause_time >= anim_post_anim_pause_time:
            # We've finished waiting, so state state and run AI
            post_anim_pause_time = 0.0
            game_state = GameState.PerformingAIlogic
            game_lock.release()
            
            ai_think()
        else:
            game_lock.release()

        return
            
    updateok = game_state == GameState.UpdatingPlayerAnimations or game_state == GameState.UpdatingAIAnimations
    game_lock.release()
    
    # Update animations
    if updateok:
        animsinprogress = False
        for entity in game_map.entities:
            if entity.animation:
                animsinprogress = True
                entity.animation.time += dt;
                if entity.animation.time > entity.animation.length:
                    entity.animation = None

        if animsinprogress:
            on_draw()
        else:
            # If no animations are in progress, then move on to the next state
            game_lock.acquire()
            ainext = game_state == GameState.UpdatingPlayerAnimations
            
            pausenext = ainext and anim_post_anim_pause_time > 0.0
            if ainext and pausenext:
                game_state = GameState.PostAnimPause
                post_anim_pause_time = 0.0
            else:
                game_state = GameState.PerformingAIlogic if ainext else GameState.WaitingForInput
                if game_state == GameState.WaitingForInput:
                    reset_action_points()
            game_lock.release()
            
            # If AI state is next, and we don't need to pause, then run AI
            if ainext and not pausenext:
                ai_think()         


                
#
#######################################
#
# on_draw()
#    
# Render a frame.
#
#######################################
#
@window.event
def on_draw():
    global game_map
	
    window.clear()

    tile_offset_x = -30
    tile_offset_y = -26
	
    entity_offset_x = -12
    entity_offset_y = -12
	
    map_radius = 4
    zmin = 1 - map_radius
    zmax = 1 - zmin

    # Draw tiles
    for coords, maptile in game_map.tiles.items():
        p = hex_to_pixel(layout, maptile.hex)
        tile_image.blit(int(p.x) + tile_offset_x, int(p.y) + tile_offset_y)

    # Draw entities
    for entity in game_map.entities:
        if entity.animation:
            if entity.animation.time >= 0:
                ex, ey = entity.animation.lerp_pos(layout)
            else:
                p = hex_to_pixel(layout, entity.animation.oldtile.hex)
                ex = int(p.x)
                ey = int(p.y)
        else:
            p = hex_to_pixel(layout, entity.maptile.hex)
            ex = int(p.x)
            ey = int(p.y)
        
        entity.image.x = ex + entity_offset_x
        entity.image.y = ey + entity_offset_y
        entity.image.draw()
        
    # UI
    action_points_text.text = 'AP: %s' % num_action_points
    action_points_text.draw()
    
    if game_state == GameState.PlayerDead:
        restart_text.draw()



#
#######################################
#
# on_mouse_motion()
#    
# Handle mouse movement.
#
#######################################
#
@window.event
def on_mouse_motion(x, y, dx, dy):
    hex = pixel_to_hex(layout, Point(x, y))
    coords = (round(hex.q), round(hex.s), round(hex.r))
	
    # Maths is not quite accurate, so find nearest if we are in an invalid hex
    if coords[0] + coords[1] + coords[2] != 0:
        hq = hex.q - round(hex.q)
        hr = hex.r - round(hex.r)
        hs = hex.s - round(hex.s)
        #print(coords[0], coords[1], coords[2], hq, hr, hs)

    selected_maptile = game_map.get_tile(coords)
    if selected_maptile:
        pass
        #print(x, y, selected_maptile.hex.q, selected_maptile.hex.r, selected_maptile.hex.s)



#
#######################################
#
# Handle input, with lambdas for
# executing entity actions.
#
#######################################
#
input_button1_down = False
input_button2_down = False
input_button3_down = False

def exec_action(pointsneeded, action):
    if num_action_points >= pointsneeded:
        return pointsneeded if action() else 0
    else:
        return 0

def handle_input(symbol, modifiers):
    global input_button1_down, input_button2_down, input_button3_down
   
    input_button1_pressed = False
    input_button2_pressed = False
    input_button3_pressed = False
    
    if symbol == key.A:
        input_button1_down = True
        input_button1_pressed = True
    if symbol == key.S:
        input_button2_down = True
        input_button2_pressed = True
    if symbol == key.D:
        input_button3_down = True
        input_button3_pressed = True
        
    #
	# Undirected attacks
	#
    if input_button1_down and input_button2_pressed:
        #
        # Knock all nearby enemies down
        #
        return exec_action(whirlwind_action_points, lambda: player.attack(None, AttackType.WHIRLWIND))

    #
	# Movement / implicit directed attacks
	#
    movedir = Hex(0, 0, 0)
    if symbol == key.NUM_9:
        movedir = hex_direction(Direction.UPRIGHT)
    elif symbol == key.NUM_6:
        movedir = hex_direction(Direction.DOWNRIGHT)
    elif symbol == key.NUM_5:
        movedir = hex_direction(Direction.DOWN)
    elif symbol == key.NUM_4:
        movedir = hex_direction(Direction.DOWNLEFT)
    elif symbol == key.NUM_7:
        movedir = hex_direction(Direction.UPLEFT)
    elif symbol == key.NUM_8:
        movedir = hex_direction(Direction.UP)
		
    maptile = player.maptile
    newhex = hex_add(maptile.hex, movedir)
	
    if newhex and not hex_equal(maptile.hex, newhex):
        coord = (newhex.q, newhex.r)
        targetmaptile = game_map.get_tile(coord)
        if targetmaptile:
            targetentity = targetmaptile.entity
            if targetentity:
                if input_button3_down:
                    #
                    # Leap over target
                    #
                    return exec_action(leap_action_points, lambda: game_map.move_entity(player, movedir, MovementType.LEAPING))                                        
                else:
                    if targetentity.align == AlignType.HOSTILE:

                        if input_button1_down:
                            #
                            # Try to push the target away one square
                            #
                            return exec_action(push_action_points, lambda: player.attack(targetentity, AttackType.PUSH))

                        else:
                            #
                            # Punch the target
                            #
                            return exec_action(punch_action_points, lambda: player.attack(targetentity, AttackType.PUNCH))                
                            
                    elif targetentity.type == AlignType.NEUTRAL:
                        #
                        # Interact with non-hostile entity
                        #
                        return exec_action(1, lambda: player.interact_with(targetentity))                

            elif input_button1_down and input_button3_down:
                #
                # Dash
                #
                return exec_action(dash_action_points, lambda: game_map.move_entity(player, movedir, MovementType.DASHING))
                
            elif not (input_button1_down or input_button2_down or input_button3_down):
                #
                # Move
                #
                return exec_action(move_action_points, lambda: game_map.move_entity(player, movedir, MovementType.WALKING))                
                
        else:
            if input_button3_down:
                #
                # Leap over void
                #
                return exec_action(leap_action_points, lambda: game_map.move_entity(player, movedir, MovementType.LEAPING))                                        
            
    return 0



#
#######################################
#
# on_key_press() / on_key_release()
# 
# Process input event, and farm out to
# handle_input().
#
#######################################
#
@window.event
def on_key_press(symbol, modifiers):
    global game_state
    
    if not game_lock.acquire(blocking=False):
        return

    if game_state == GameState.PlayerDead:
        if symbol == key.R:
            setup_map(map_radius)
            game_state = GameState.WaitingForInput
            game_lock.release()
            return
            
    handleok = game_state == GameState.WaitingForInput
    game_lock.release()

    if handleok:
        if handle_input(symbol, modifiers):
            use_action_points(1)
            
            game_lock.acquire()
            game_state = GameState.UpdatingPlayerAnimations
            game_lock.release()
    
@window.event
def on_key_release(symbol, modifiers):
    global input_button1_down, input_button2_down, input_button3_down

    if symbol == key.A:
        input_button1_down = False
    elif symbol == key.S:
        input_button2_down = False
    elif symbol == key.D:
        input_button3_down = False



#
#######################################
#
# ai_think()
# 
# Handle entity AI after the player has
# moved.
#
#######################################
#    
def ai_think():
    global game_state
    
    game_lock.acquire()
    updateok = game_state == GameState.PerformingAIlogic
    game_lock.release()

    if updateok:
        game_lock.acquire()
        if game_map.update_entities(player):
            game_state = GameState.UpdatingAIAnimations
        else:
            game_state = GameState.PlayerDead
        game_lock.release()



#
#######################################
#
# Entrypoint.
#
#######################################
#    
if __name__ == '__main__':

    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    try:
        setup_map(map_radius)
	
        pyglet.clock.schedule(update_frame, 1/10.0)
        pyglet.app.run()
    except Exception:
        type, value, traceback = sys.exc_info()
        print(type, value, traceback)
        sys.exit(1)
    else:
        sys.exit(0)