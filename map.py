import random

from hex import *
from etypes import *
from entity import Entity
from animation import Animation

# Tile class
class MapTile:
    def __init__(self, map, x, z, entity=None):
        self.map = map
        self.x = x
        self.z = z
        self.hex = Hex(x, z, -x - z)
        self.entity = entity
		
    def blocked(self):
        return self.entity is not None

class Map:
    def __init__(self):
        self.tiles = dict()
        self.entities = list()
	   
    def has_tile(self, coords):
        return coords in self.tiles
	  
    def get_tile(self, coords):
        return self.tiles.get(coords)

    def add_tile(self, x, z):
        self.tiles[(x, z)] = MapTile(self, x, z)	   

    def add_entity(self, x, z, type, align, state, on_die):
        coord = (x, z)
	
        if coord in self.tiles and not self.tiles[coord].entity:
            maptile = self.tiles[coord]

            entity = Entity(maptile, type, align, state, on_die)
            maptile.entity = entity
			
            self.entities.append(entity)
            return entity
        else:
            return None

    def remove_entity(self, entity):
        entity.maptile.entity = None
        self.entities.remove(entity)
	
    def move_entity(self, entity, dir, movetype, animdelay=0):
        newhex = hex_add(entity.maptile.hex, dir)

        # Two-tile moves
        if movetype in [MovementType.LEAPING, MovementType.DASHING]:
            if movetype == MovementType.DASHING:
                coord = (newhex.q, newhex.r)
                if coord not in self.tiles or self.tiles[coord].entity:
                    return False
                
            newhex = hex_add(newhex, dir)

        coord = (newhex.q, newhex.r)
	
        if coord in self.tiles and not self.tiles[coord].entity:
            newtile = self.tiles[coord]
            newtile.entity = entity
            entity.maptile.entity = None
            
            # Animation
            entity.animation = Animation(movetype, entity.maptile, newtile, animdelay)
            entity.maptile = newtile
            return True
        else:
            return False
			
    def random_free_direction(self, coords):
        tile = self.get_tile(coords)
        choices = list()

        for i in range(6):
            dir = hex_direction(i)
            targethex = hex_add(tile.hex, dir)
            targettile = self.get_tile((targethex.q, targethex.r))
            if targettile and not targettile.blocked():
                choices.append(dir)
			
        if len(choices) == 0:
            return None
        else:
            return random.choice(choices)

    def has_line_of_sight(self, coords, direction, distance):
        tile = self.get_tile(coords)
        
        for i in range(distance):
            targethex = hex_add(tile.hex, direction)
            tile = self.get_tile((targethex.q, targethex.r))
            
            if not tile:
                return False
                
        return True
        
    def sees_entity(self, coords, distance, entity):
        for i in range(6):
            tile = self.get_tile(coords)
            for j in range(distance):
                targethex = hex_add(tile.hex, hex_direction(i))
                tile = self.get_tile((targethex.q, targethex.r))
            
                if not tile:
                    break
                elif tile.entity == entity:
                    return i

        return -1
    
            
    def update_entities(self, player):
        for entity in self.entities:
            if entity == player or entity.state != EntityState.CONSCIOUS:
                continue

            if entity.incapacitation_timer == 0:
                coords = (entity.maptile.hex.q, entity.maptile.hex.r)
                
                if entity.type == EntityType.ENEMY1:
                    # Move towards player if in sight, attack if close enough
                    playerdir = self.sees_entity(coords, 3, player)
                    if playerdir >= 0:
                        if hex_distance(entity.maptile.hex, player.maptile.hex) < 2:
                            if entity.attack(player, AttackType.CLAW):
                                # Check if player is dead
                                if player.state == EntityState.DEAD:
                                    return False
                        else:
                            self.move_entity(entity, hex_direction(playerdir), MovementType.WALKING)

            if entity.incapacitation_timer > 0:
                entity.incapacitation_timer -= 1
        
        return True